:: discasset quick runner

:: extract all cdn-hosted assets from webpack chunks
node cdnAssets -skip
:: extract assets from css files
node cssAssets -skip
:: extract svg icons from webpack chunks
node svgAssets -skip
:: extract lotties from webpack chunks
node lottieAssets -skip
