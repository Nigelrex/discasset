const xml = require('xml')
const fs = require('fs');
const esprima = require('esprima');
const crypto = require('crypto');

const superagent = require("superagent")


// -- Constants --

//Patterns
const pattern = /createElement\("svg".*?.\.displayName/gs
const namePattern = /createElement\("svg".*?.\.displayName="([A-z0-9_]*)"?/gs

//Default Export Color
const color = 'B9BBBE'

//Load JSON storage file into variable
var storage = fs.readFileSync('./.data/assets.json', 'utf-8')
var jsonstorage = JSON.parse(storage)

//CONSTANTS
const stylesheet = /stylesheet" href="(.*?\.css)/g //Regex to get the stylesheet hash/url
const script = /script src="(\/assets\/.{20}\.js)/g //Regex to get all the scripts on the login page
const loaderRegex = /.\..=.=>\((\{.*?\})\[./gs //Regex to extract the app scripts from the chunkloader
const jsonFix = /([0-9]*):/g //Regex to fix json from the chunkloader so we can easily parse it
const canary = 'https://canary.discord.com/login' //Entrypoint to get all the app data from
const assetHost = 'https://canary.discord.com' //Host for assets

// --           --

let debug = false
let stop = false

//Process Argument Handling
const processArgs = process.argv.slice(2);
if(processArgs.includes('-debug')){
  debug = true
}
if(processArgs.includes('-skip')){
  stop = true
}

const types = ["path", "defs", "circle", "rect", "polygon", "ellipse", "linearGradient", "mask", "clipPath"]

function processElement(element){
  const type = element.arguments[0].value
  if(type == "g"){  //Groups
    let parsedElement = {
      g: [],
      _attr: {}
    }
    element.arguments[1].properties.forEach(function(property){
      parsedElement._attr[property.key.name] = property.value.value
      //Special Cases
      if(property.key.name == "fill"){
        parsedElement._attr[property.key.name] = color
      }
    })
    element.arguments.splice(1, element.arguments.length).forEach(function(elem){
      if(elem.type == "CallExpression"){
        let processedElement = processElement(elem)
        parsedElement.g.push(processedElement)
      }
    })
    return parsedElement
  } else if(types.includes(type)){  //Regular Elements
    let parsedElement = {
      _attr: {}
    }
    element.arguments[1].properties.forEach(function(property){
      parsedElement._attr[property.key.name] = property.value.value
      //Special Cases
      if(property.key.name == "fill"){
        parsedElement._attr[property.key.name] = color
      }
    })
    parsed = {}
    parsed[type] = parsedElement
    return parsed;
  } else { //Unknown thing, return {} so hopefully nothing breaks
    return {}
  }
}

//Extract the data and return svg element in XML
function getVector(func){
  const functionData = esprima.parseScript(func) //Parse the function
  let viewBox
  //Get the viewBox
  functionData.body[0].expression.arguments[1].arguments.forEach(function(box){
    if(box.properties){
      box.properties.forEach(function(view){
        if(view.key.name == "viewBox"){
          viewBox = view.value.value 
        }
      })
    }
  })
  let vector = [
    {
      svg: [
        {
          _attr: { xmlns: "http://www.w3.org/2000/svg", viewBox: viewBox, comment: "discasset/1.2" }
        }
      ]
    }
  ]
  functionData.body[0].expression.arguments.splice(1, functionData.body[0].expression.arguments.length).forEach(function(element){
    if(element.callee){
      vector[0].svg.push(processElement(element))
    }
  })
  return xml(vector, true)
}


async function svgassets(){
  let site;
  let newSaves = 0;
  try{
    site = await superagent.get(canary)
    const match = site.text.match(script) 
    scriptData = await superagent.get(`${assetHost}${match[0].replace(`script src="`, '')}`)
    scripts = loaderRegex.exec(scriptData.body)
    scriptData = scripts[1].toString().replace(jsonFix, "\"$1\":")
    scripts = JSON.parse(scriptData)
    scripts["0"] = match[3].replace(`script src="`, '').replace('.js', '').replace('/assets/', '')
    let processed = 0;
    let counter = 0;
    let success = 0;
    let total = 0;
    let newSaves = 0;
    Object.values(scripts).forEach(async function(scr, i){
      data = await superagent.get(`${assetHost}/assets/${scr}.js`)
      waba = await extract(data.body.toString(), i)
      counter += waba.counter
      success += waba.success
      total += waba.matches
      newSaves += waba.new
      processed += 1
      if(processed == Object.values(scripts).length){
        console.log(`Exported ${success} out of ${total} detected vector graphics. Press any key to exit.`)
        if(newSaves !== 0){
          console.log(`Saved \x1b[36m${newSaves}\x1b[0m new assets.`)
          fs.writeFileSync('./.data/assets.json', JSON.stringify(jsonstorage, null, 2), 'utf8') //Store amount of vectors we were able to extract
        }
        if(stop == false){
          process.stdin.setRawMode(true);
          process.stdin.resume();
          process.stdin.on('data', process.exit.bind(process, 0));
        }
        if(stop == false){
          process.stdin.setRawMode(true);
          process.stdin.resume();
          process.stdin.on('data', process.exit.bind(process, 0));
        }
      }
    })
  } catch(e){
    console.log('[svgassets] Failed to download discord data:')
    console.log(e)
  }
}

//extract | extract svgs from file
async function extract(data, i){
  let matches = data.match(pattern)   // Match patterns to extract
  let names = data.match(namePattern)//  Functions and names
  let counter = 0
  let success = 0
  let loop = 0
  let newSaves = 0;
  if(matches == null){return {counter: 0, success: 0, matches: 0, new: 0};}
  matches.forEach(function(asset){
    loop++
    try{
      convert = asset.replace(/(\}|\};).\.displayName/, '')
      svgComponent = getVector(convert) //get the evaluated function result
      let fileName;
      hash = crypto.createHash('md5').update(svgComponent).digest('hex'); //Get a hash of the vector
      try{
        fileName = names[loop-1].replace(namePattern, "$1") //Search for the corresponding extracted function with the name
        if(fileName == names[loop-1]){
          fileName = hash
        } else {
          fileName = `${fileName}_(${hash})`
        }
      } catch(e){
        fileName = hash
      }
      if(!fs.existsSync(`./out/svg/${fileName}.svg`)){
        fs.writeFileSync(`./out/svg/${fileName}.svg`, svgComponent);
      }
      if(!jsonstorage.assets.svg.includes(hash)){
        jsonstorage.assets.svg.push(hash)
        newSaves += 1
      }
      counter += 1
      success += 1
    } catch(e){
      if(debug == true){
        console.log(asset)
        console.log(names[loop-1].replace(namePattern, "$1"))
        console.log(e)
      }
    }
  })
  return {counter: counter, success: success, matches: matches.length, new: newSaves}
}

(async () => {
  await svgassets()
})();