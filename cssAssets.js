const superagent = require('superagent')
const fs = require('fs');
const http = require('https');

async function dl(url, name, path){
  if(!fs.existsSync(`${path}${name}`)){
    try{
      const dlAsset = await superagent.get(url)
      fs.writeFileSync(`${path}${name}`, dlAsset.body, 'utf8')
    }catch(e){} //Asset cant be downloaded for some reason
  }
}

let stop = false

//Process Argument Handling
const processArgs = process.argv.slice(2);
if(processArgs.includes('-skip')){
  stop = true
}

//Constants
const pattern = /url\(\/assets\/.{32}\.(svg|png|jpg|gif|mp4|ttf|otf|woff|woff2)/g
const cdn = "https://canary.discord.com/assets/"


//CONSTANTS
const stylesheet = /stylesheet" href="(.*?\.css)/g //Regex to get the stylesheet hash/url
const script = /script src="(\/assets\/.{20}\.js)/g //Regex to get all the scripts on the login page
const loaderRegex = /.\..=.=>\((\{.*?\})\[./gs //Regex to extract the app scripts from the chunkloader
const jsonFix = /([0-9]*):/g //Regex to fix json from the chunkloader so we can easily parse it
const buildRegex = /\(e="(.{40})"\)\?void 0:e\.substr\(0,7\)/g //Regex to get the build hash
const buildNumberRegex = /Build Number: ([0-9]*),/g //Regex to get the build number
const canary = 'https://canary.discord.com/login' //Entrypoint to get all the app data from
const assetHost = 'https://canary.discord.com' //Host for assets
const environment = "canary"

async function cssassets(){
  const storage = fs.readFileSync('./.data/assets.json') // Read and parse jsonstorage
  var jsonstorage = JSON.parse(storage) 
  let site;
  try{
    site = await superagent.get(canary)
    const match = site.text.match(script) 
    scriptData = await superagent.get(`${assetHost}${match[0].replace(`script src="`, '')}`)
    scripts = loaderRegex.exec(scriptData.body)
    const matches = stylesheet.exec(site.text)
    style = await superagent.get(`${assetHost}${matches[1]}`)
    await extract(style.text)
  } catch(e){
    console.log('[Error] Failed to download discord data:')
    console.log(e)
  }
}

async function extract(data){
  var storage = fs.readFileSync('./.data/assets.json', 'utf-8')
  var jsonstorage = JSON.parse(storage)
  const matches = data.match(pattern)
  let newSaves = 0;
  matches.forEach(function(asset){
    ext = asset.replace(/url\(\/assets/g, '')
    dl(`${cdn}${ext}`, `${ext}`, `./out/assets/`)
    assetId = ext.replace(/\/(.{32}).(svg|png|jpg|gif|mp4|ttf|otf|woff|woff2)/g, '$1')
    if(!jsonstorage.assets.cdn.includes(assetId)){
      jsonstorage.assets.cdn.push(assetId)
      newSaves++
    }
  })
  const countData = fs.readFileSync('./.data/assets.json')
  counting = JSON.parse(countData)
  console.log(`Downloaded ${jsonstorage.assets.cdn.length} assets. Press any key to exit.`)
  if(newSaves !== 0){
    console.log(`Saved \x1b[36m${newSaves}\x1b[0m new assets.`)
    fs.writeFileSync('./.data/assets.json', JSON.stringify(jsonstorage, null, 2), 'utf8') //Store version
  }
  if(stop == false){
    process.stdin.setRawMode(true);
    process.stdin.resume();
    process.stdin.on('data', process.exit.bind(process, 0));
  }
}

(async () => {
  await cssassets()
})();
