const superagent = require("superagent")
const fs = require('fs')

//CONSTANTS
const stylesheet = /stylesheet" href="(.*?\.css)/g //Regex to get the stylesheet hash/url
const script = /script src="(\/assets\/.{20}\.js)/g //Regex to get all the scripts on the login page
const loaderRegex = /.\..=.=>\((\{.*?\})\[./gs //Regex to extract the app scripts from the chunkloader
const jsonFix = /([0-9]*):/g //Regex to fix json from the chunkloader so we can easily parse it
const buildRegex = /\(e="(.{40})"\)\?void 0:e\.substr\(0,7\)/g //Regex to get the build hash
const buildNumberRegex = /Build Number: ([0-9]*),/g //Regex to get the build number
const canary = 'https://canary.discord.com/login' //Entrypoint to get all the app data from
const assetHost = 'https://canary.discord.com' //Host for assets
const environment = "canary"

async function download(){
  const storage = fs.readFileSync('./.data/assets.json') // Read and parse jsonstorage
  var jsonstorage = JSON.parse(storage) 
  let site;
  try{
    //check version
    ver = await superagent.get(`${assetHost}/assets/version.${environment}.json`)
    if(jsonstorage.build.id == ver.body.hash){
      console.log(`[Download] Version ${jsonstorage.build.number} (${jsonstorage.build.id}) is already downloaded. Press any key to exit.`)
      process.stdin.setRawMode(true);
      process.stdin.resume();
      process.stdin.on('data', process.exit.bind(process, 0));
    }
    site = await superagent.get(canary)
    const match = site.text.match(script) 
    scriptData = await superagent.get(`${assetHost}${match[0].replace(`script src="`, '')}`)
    scripts = loaderRegex.exec(scriptData.body)
    scriptData = scripts[1].toString().replace(jsonFix, "\"$1\":")
    scripts = JSON.parse(scriptData)
    //The client script
    clientScript = await superagent.get(`${assetHost}${match[3].replace(`script src="`, '')}`)
    fs.writeFileSync('./.data/client/00', clientScript.body, 'utf8')
    const buildId = buildNumberRegex.exec(clientScript.body)
    let mainScript = ""
    Object.values(scripts).forEach(async function(scr){
      if(mainScript !== ""){
        return;
      }
      data = await superagent.get(`${assetHost}/assets/${scr}.js`)
      const buildData = buildRegex.exec(data.body)
      if(buildData){
        mainScript = data.body
        if(jsonstorage.build.id == buildData[1]){
        } else {
          jsonstorage.build.id = buildData[1]
          jsonstorage.build.number = buildId[1]
          fs.writeFileSync('./.data/assets.json', JSON.stringify(jsonstorage, null, 2), 'utf8')
          console.log(`[Download] Downloading files for build ${jsonstorage.build.number} (${jsonstorage.build.id.substr(0, 7)})...`)
          fs.writeFileSync('./.data/client/01', mainScript, 'utf8')
          console.log('[Download] JavaScript Downloaded.')
          //Stylesheet Download
          const matches = stylesheet.exec(site.text)
          style = await superagent.get(`${assetHost}${matches[1]}`)
          fs.writeFileSync('./.data/client/10', style.text, 'utf8')
          console.log('[Download] Stylesheet Downloaded.')
          console.log(`[Download] Successfully downloaded files for build ${jsonstorage.build.number} (${jsonstorage.build.id}). Press any key to exit.`)
          process.stdin.setRawMode(true);
          process.stdin.resume();
          process.stdin.on('data', process.exit.bind(process, 0));
        }
      }
    })
  } catch(e){
    console.log('[Error] Failed to download discord data:')
    console.log(e)
  }
}

(async () => {
  await download()
})();
